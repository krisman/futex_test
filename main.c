#define _GNU_SOURCE
#include <unistd.h>
#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<linux/futex.h>
#include<sys/syscall.h>
#include<sys/time.h>
#include<stdbool.h>
#include<string.h>
#include<errno.h>

#define NR_PROD  127

#define MAX_CRED 100
#define PACE 1000
#define MAX_PRODUCED (MAX_CRED * PACE)

#define FUTEX_WAIT_MULTIPLE (13|128)

struct prod_info {
	pthread_t thrd;
	int id;
	int lock;
	int count;
	int res;
	int done;
	int consumed;
} prods[NR_PROD];

struct {
	unsigned *uaddr;
	unsigned val;
	unsigned bitset;
} fwb[NR_PROD];

int futex(int *uaddr, int futex_op, int val,
          const struct timespec *timeout,   /* or: uint32_t val2 */
          int *uaddr2, int val3)
{
	return syscall(__NR_futex, uaddr, futex_op,
		       val, timeout, uaddr2, val3);
}

pthread_t cons_thrd;

//void lock_me(l, thrd)  __lock_me(lock,  __FUNCTION__, __LINE__)
void lock_me(unsigned int *lock, int thrd)
{
	bool r;
	unsigned int expected = 0;

	while (!__atomic_compare_exchange_n(lock, &expected, 1, false,
						__ATOMIC_SEQ_CST,
					    __ATOMIC_SEQ_CST)) {
		futex(lock, FUTEX_WAIT|128, 1, NULL, NULL, 0);
	}

//	printf("acquired lock %x of %i producer oldval=%d val=%d\n", lock, thrd, expected, *lock);
}

#define unlock_me(x, thrd) _unlock_me(x, thrd,  __FUNCTION__, __LINE__)
void _unlock_me(unsigned int *lock, int thrd, const char *func,  int line)
{
	if (!*lock) {
		printf("UNLOCKING NOT LOCKED lock: %s thrd=%d\n",
		       func, thrd);
		exit(1);

	}
//	printf("release lock: %s: thrd=%d\n", func, thrd);

	*lock = 0;
	futex(lock, FUTEX_WAKE_PRIVATE, 1, NULL, NULL, 0);
}

#ifdef FUTEX_WAIT_MULTIPLE
/* int lock_any_of() */
/* { */
/* 	unsigned int expected = 0; */
/* 	bool locked = false; */
/* 	int remain = 0; */
/* 	int ret; */
/* 	int need_wait; */
/* 	int i; */
/* 	int thrd_idx[NR_PROD]; */

/* retry: */
/* 	for (i = 0 ; i < NR_PROD; i++) { */
/* 		struct prod_info *thrd = &prods[i]; */

/* 		if (!thrd->done && !thrd->count) { */
/* 			need_wait = 1; */
/* 			continue; */
/* 		} */

/* 		if (thrd->consumed < MAX_PRODUCED) { */
/* 			fwb[remain].uaddr = &thrd->lock; */
/* 			fwb[remain].bitset = 0xffffffff; */
/* 			fwb[remain].val = 0; */
/* 			thrd_idx[remain] = i; */
/* 			remain += 1; */
/* 		} */
/* 	} */

/* 	if (!remain) { */
/* 		if (need_wait) { */
/* 			need_wait = 0; */
/* 			usleep(5); */
/* 			goto retry; */
/* 		} */
/* 		return -1; */
/* 	} */

/* 	while (true) { */
/* 		for (i = 0 ; i < remain; i++) { */
/* 			fwb[i].val = 0; */
/* 			locked = __atomic_compare_exchange_n( */
/* 				fwb[i].uaddr, */
/* 				&(fwb[i].val), 1, false, */
/* 				__ATOMIC_SEQ_CST, */
/* 				__ATOMIC_SEQ_CST); */

/* 			if (locked) { */
/* 		/\* 		printf(  *\/ */
/* 		/\* "consumer acquired lock %x thrd=%d consumed=%d oldval=%d val=%d\n", *\/ */
/* 		/\* 		       fwb[i].uaddr, *\/ */
/* 		/\* 		       thrd_idx[i], *\/ */
/* 		/\* 		       prods[thrd_idx[i]].consumed, *\/ */
/* 		/\* 		       fwb[i].val, *\/ */
/* 		/\* 		       *fwb[i].uaddr); *\/ */
/* 				return thrd_idx[i]; */
/* 			} */
/* 		} */

/* 		printf("remain=%d\n", remain); */

/* 		ret = futex((unsigned *)fwb, */
/* 			    FUTEX_WAIT_MULTIPLE, */
/* 			    remain, NULL, 0, 0); */
/* 		if (ret == -1 && errno != EAGAIN) { */
/* 			printf("\tfutex-wait-multiple failed with: %d %s\n", */
/* 			       ret, strerror(errno)); */
/* 		}  else if (ret>=0) { */
/* 			fwb[ret].val = 0; */
/* 			locked = __atomic_compare_exchange_n( */
/* 				fwb[ret].uaddr, */
/* 				&(fwb[ret].val), 1, false, */
/* 				__ATOMIC_SEQ_CST, */
/* 				__ATOMIC_SEQ_CST); */
/* 			if (locked) */
/* 				return thrd_idx[ret]; */
/* 		} */
/* 	} */
/* } */

int lock_any_of()
{
	unsigned int expected = 0;
	bool locked = false;

	int ret;
	int i;

	while (true) {
		int done = 0;
		int need_wait = 0;

		for (i = 0 ; i < NR_PROD; i++) {
			fwb[i].uaddr = &prods[i].lock;
			fwb[i].bitset = 0xffffffff;
			fwb[i].val = 0;

			locked = __atomic_compare_exchange_n(
				fwb[i].uaddr,
				&(fwb[i].val), 1, false,
				__ATOMIC_SEQ_CST,
				__ATOMIC_SEQ_CST);

			if (locked) {
				if (!prods[i].count) {
					if (prods[i].consumed == MAX_PRODUCED)
						done++;
					unlock_me(&prods[i].lock, prods[i].id);
					continue;
				}
				return i;
			}
		}
		if (done == NR_PROD)
			return -1;
		if (need_wait == NR_PROD) {

			need_wait = 0;
//			usleep(50);
//			continue;
		}

		ret = futex((unsigned *)fwb,
			    FUTEX_WAIT_MULTIPLE,
			    NR_PROD, NULL, 0, 0);
		if (ret == -1 && errno != EAGAIN) {
			printf("\tfutex-wait-multiple failed with: %d %s\n",
			       ret, strerror(errno));
		}  else if (ret>=0) {
			fwb[ret].val = 0;
			locked = __atomic_compare_exchange_n(
				fwb[ret].uaddr,
				&(fwb[ret].val), 1, false,
				__ATOMIC_SEQ_CST,
				__ATOMIC_SEQ_CST);
			if (locked)
				return ret;
		}
	}
}

#endif

void work(struct prod_info *prod) {
	int i;
	for (i = 0 ; i < PACE; i++) {
			usleep(5);
			prod->count ++;
	}
//		printf("produced %d\n", prod->count);

}


void producer(void *x)
{
	struct prod_info *thrd = x;
	int i;
	for (i=0; i < MAX_CRED ; i++) {
		lock_me(&thrd->lock, thrd->id);
		work(thrd);
		unlock_me(&thrd->lock, thrd->id);
		usleep(10);
	}

	thrd->done = 1;
	printf("Producer %d is done. Byt\n", thrd->id);
}

int consume_one_thread(struct prod_info *thrd, int i)
{
	if (thrd->count) {
		thrd->consumed += thrd->count;
		printf("consuming %d entries from %d\n", thrd->count, i);
		thrd->count = 0;
	}

	return thrd->done;
}


void consumer(void *x)
{
	int i;
	int done = 0;
	int total = 0;


#ifdef FUTEX_WAIT_MULTIPLE
	for (i = 0 ; i < NR_PROD; i++) {
			struct prod_info *thrd = &prods[i];
		fwb[i].uaddr = &thrd->lock;

	}
	while ((i = lock_any_of()) != -1) {
		struct prod_info *thrd = &prods[i];
		consume_one_thread(thrd, i);
		unlock_me(&thrd->lock, i);
	}

#else
	
	while (!done) {
		done = 1;
		for (i = 0 ; i < NR_PROD; i++) {
			struct prod_info *thrd = &prods[i];

			lock_me(&thrd->lock, i);
			if (!consume_one_thread(thrd, i))
				done = 0;
			unlock_me(&thrd->lock, i);
			usleep(400);
		}
	}
#endif

	for (i = 0 ; i < NR_PROD; i++) {
		total += prods[i].consumed;
	}
	printf("Bye. Consumed %d\n", total);
	for (i = 0 ; i < NR_PROD; i++)
		printf("\t%d => %d\n", i, prods[i].consumed);
}

int main ()
{
	int r, i;

	for (i = 0 ; i < NR_PROD; i++) {
		prods[i].lock = 0;
		prods[i].id = i;
		r = pthread_create(&prods[i].thrd, NULL,
				   (void*)producer, (void*)&prods[i]);
		if (r) {
			printf("failed ith producer creation\n %s",
			       strerror(r));
			return 1;
		}
	}

	r = pthread_create(&cons_thrd, NULL, (void*)consumer, NULL);
	if (r) {
		printf("failed consumer creation\n %s", strerror(r));
		return 1;
	}

	for (i = 0 ; i < NR_PROD; i++)
		pthread_join(prods[i].thrd, NULL);

	pthread_join(cons_thrd, NULL);
}

